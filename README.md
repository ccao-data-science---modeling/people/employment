### :warning: Development for this project has moved to a [new repository on GitHub](https://github.com/ccao-data/people) :warning:

This repository contains:

* Documents, forms, and issue templates for onboarding and offboarding Data Science staff
* Fillable forms for performance evaluation of Data Science staff
* Forms pertaining to employment actions
* Documents related to hiring plans and processes
