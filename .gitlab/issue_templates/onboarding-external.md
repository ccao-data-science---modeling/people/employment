Thank you for partnering with the Cook County Assessor's Data Department. Please read the content below to get started. 

## Developer Engagement Documents

Before we can collaborate on a project, we need a collaborator agreement. Please review the [collaborator wiki page](https://gitlab.com/groups/ccao-data-science---modeling/-/wikis/People/Contributing) for an overview of the documents required by our external collaboration program. Please email the completed documents to the current CDO (Chief Data Officer). 

## Accounts to Create

* [GitLab](https://gitlab.com/) - Needed to contribute to the CCAO codebase. If you already have an existing account, feel free to use it. After completing the collaborator agreement, external contributors will be added to the CCAO GitLab group for the duration of their project. All external collaborators will be added to GitLab as [Reporters](https://docs.gitlab.com/ee/user/permissions.html).

## Teams

The Data Department uses Microsoft Teams as its primary method of communication. External collaborators can be added to CCAO Teams group as guests. For an invite, please email the current CDO.

## Bookmarks

Additionally, take a moment to bookmark all the links marked with a :exclamation: under the "Useful Bookmarks" section of the [Resources](https://gitlab.com/groups/ccao-data-science---modeling/-/wikis/Handbook/Resources#useful-bookmarks) page.

/assign @sweatyhandshake @dfsnow
