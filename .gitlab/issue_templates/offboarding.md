> :warning: This page only applies to CCAO Data Department interns, capstone students, and fellows who are leaving at the end of their quarter/semester.

If you're reading this, then it's finally time to say goodbye. :sob: We're sad to see you go, but hope you've gained something from being a part of the Data Department! After leaving, you'll automatically lose access to your CCAO email address, CCAO private GitLab repos, the VPN, and Teams. Here are the things you need to do before you leave:

# Checklist

- [ ] Return any CCAO equipment (if applicable). Laptops should be returned directly to the IT department.
- [ ] Schedule an informal exit chat with your supervisor.
- [ ] Email [HR](assessor.ccaohr@cookcountyil.gov) to confirm your last day.
- [ ] Delete any stored CCAO read-only database credentials on your local machine (if applicable).
- [ ] Email the Chief Data Officer, Directors, and Senior Data Scientists: 
  - A reminder to change the permissions on your GitLab account
  - A reminder to revoke any private API/deploy keys
  - A reminder to revoke any relevant AWS permissions 
  - Your contact information, in case we need to follow-up on something you worked on
  - A reminder to add you to the [list of former interns and employees used to generate model names](https://gitlab.com/ccao-data-science---modeling/packages/ccao/-/blob/master/data-raw/ccao_ids.R)
